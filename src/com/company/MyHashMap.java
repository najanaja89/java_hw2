package com.company;

import java.util.LinkedList;

class MyHashMap {
    private LinkedList<Integer> myNode;
    public MyHashMap() {
        myNode = new LinkedList<Integer>();
    }

    /** value will always be non-negative. */
    void put(int key, int value) {
        if (myNode.isEmpty())
        {
            for (int i = 0; i < 1000000; i++) {
                myNode.add(-1);
            }
        }
        myNode.set(key, value);
    }

    /** Returns the value to which the specified key is mapped, or -1 if this map contains no mapping for the key */
    int get(int key) {
        if(myNode.get(key) == -1) return -1;
        return myNode.get(key);
    }

    /** Removes the mapping of the specified value key if this map contains a mapping for the key */
    void remove(int key) {
        myNode.set(key, -1);
    }
};
